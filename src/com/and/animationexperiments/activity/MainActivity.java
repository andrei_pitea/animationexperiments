package com.and.animationexperiments.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.and.animationexperiments.R;

public class MainActivity extends Activity implements View.OnClickListener {

    protected Button mActivityAnimations;
    protected Button mListViewRemovalAnimations;
    protected Button mLayoutTransitions;
    protected Button mUsingShaders;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setActionBar();
        setViews();
    }

    private void setActionBar() {
        getActionBar().setTitle("Animation Experiments");
    }

    private void setViews() {
        mActivityAnimations = (Button) findViewById(R.id.activity_animations);
        mListViewRemovalAnimations = (Button) findViewById(R.id.listview_removal);
        mLayoutTransitions = (Button) findViewById(R.id.layout_transitions);
        mUsingShaders = (Button) findViewById(R.id.using_shaders);

        mActivityAnimations.setOnClickListener(this);
        mListViewRemovalAnimations.setOnClickListener(this);
        mLayoutTransitions.setOnClickListener(this);
        mUsingShaders.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_animations:
                final Intent activityAnimationsIntent = new Intent(MainActivity.this, ActivityAnimations.class);
                startActivity(activityAnimationsIntent);
                break;
            case R.id.listview_removal:
                final Intent listViewRemovalAnimationsIntent = new Intent(MainActivity.this, ListViewRemovalAnimations.class);
                startActivity(listViewRemovalAnimationsIntent);
                break;
            case R.id.layout_transitions:
                final Intent layoutTransitionsIntent = new Intent(MainActivity.this, LayoutTransChanging.class);
                startActivity(layoutTransitionsIntent);
                break;
            case R.id.using_shaders:
                final Intent usingShadersIntent = new Intent(MainActivity.this, SpotlightActivity.class);
                startActivity(usingShadersIntent);
                break;
        }
    }
}
